#ifndef _SYLLS_HPP_
#define _SYLLS_HPP_

#include <iostream>

//Header containing syllables
std::string syllables[27]
{
	"ka", "tu", "mi", "te", "ku", "lu", "ji",
	"ri", "ki", "zu", "me", "ta", "rin", "to",
	"mo", "no", "ke", "shi", "ari", "chi", "do",
	"ru", "mei", "na", "fu", "zi"
};

/*
std::string syllables2[72]
{
	"a", "i", "u", "e", "o", "ka", "ki",
	"ku", "ke" , "ko", "sa", "shi", "su",
	"se", "so", "ga", "gi", "gu", "ge", "go",
	"za", "ji", "zu", "ze", "zo", "ta", "chi",
	"tsu", "te", "to", "ha", "hi", "fu", "he",
	"ho", "da", "ji", "zu", "de", "do", "ba",
	"bi", "bu", "be", "bo", "ma", "mi", "mu",
	"me", "mo", "pa", "pi", "pu", "pe", "po",
	"na", "ni", "nu", "ne", "no", "ra", "ri",
	"ru", "re", "ro", "ya", "wa", "yu", "n",
	"wo", "yo"
};
*/

#endif // _SYLLS_HPP_
