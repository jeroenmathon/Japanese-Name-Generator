#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "sylls.hpp"

int main(int argc, char **argv)
{
	//Check if program is used correctly
	if(argc == 1)
	{
		 std::cout << "Usage:\n"
		 << "       Name\\ Generator {LENGTH} {Syllables untill Space(optional)}\n";
		 return -1;
	}

	int mLength = atoi(argv[1]); //Max name length
	int nSpace(0); //Syllables untill we place a spacebar

	//Check if we want spaces
	if(argc > 2)  nSpace = atoi(argv[2]);  //Length untill space

	//Current the random number
	int cnum;

	//Random seed
	srand(time(NULL));;

	//The main execution loop
	for(int i(0); i < mLength ; i++)
	{
		//Generate random number
		cnum = rand() % 27;
		//Print out a syllable
		std::cout << syllables[cnum];

		//Place a space if we want that
		if(i == nSpace-1 && argc > 2) std::cout << " ";
	}

	std::cout << "\n";

	return 0;
}
